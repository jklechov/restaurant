package com.restaurant.table;

import java.util.ArrayList;
import java.util.List;

public class Bill {
    private String waiterName;
    private int tableNumber;
    private List<MenuItem> orderedItems;
    private double discount;

    public Bill(String waiterName, int tableNumber){
        this.waiterName=waiterName;
        this.tableNumber=tableNumber;
        this.orderedItems= new ArrayList<MenuItem>();
    }
    public void addOrderedItem(MenuItem item){
        orderedItems.add(item);
    }
    public void setDiscount(double discount){
        this.discount=discount;
    }
    public double calculateTotalAmount(){
        double total=0;
        for (MenuItem item : orderedItems){
            total +=item.getPrice();
        }
        total -= discount;
        return total;
    }
    public void printBill(){
        System.out.println("Waiter: " + waiterName);
        System.out.println("Table Number: " + tableNumber);
        System.out.println("----------------------------------------");
        System.out.println("Ordered Items");
        for (MenuItem item : orderedItems){
            System.out.println("- " + item.getName() + ": " + item.getPrice() + "$");

        }
        System.out.println("----------------------------------------");
        System.out.println("Sicount: " + discount + "$");
        System.out.println("----------------------------------------");
        System.out.println("Total Amount to Pay: " + calculateTotalAmount() + "$");
        System.out.println("----------------------------------------");
        System.out.println("\nYou are always welcome in our");
        System.out.println("restaurant, hope everything was fine");
    }
}
